package core.main.inventory;

/**
 * @author doc
 */
public enum ItemTier {
	COMMON,
	UNCOMMON,
	RARE,
	MYTHICAL,
	LEGENDARY
}
