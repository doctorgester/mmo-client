package core.main.inventory;

/**
 * @author doc
 */
public interface ItemFilter {
	public boolean doesItemPass(Item item);
}
