package core.ui.inventory;

import core.main.inventory.ItemFilter;

/**
 * @author doc
 */
public interface FilterListener {

	public void buttonPressed(FilterButton button);
}
