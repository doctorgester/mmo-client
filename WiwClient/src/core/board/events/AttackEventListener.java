package core.board.events;

public interface AttackEventListener {
	public void onAttack(AttackEventContext context);
}
