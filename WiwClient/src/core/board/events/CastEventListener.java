package core.board.events;

public interface CastEventListener {
	public void onCast(CastEventContext context);
}
