package core.board.events;

public interface DamageEventListener {
	public void onDamageTaken(DamageEventContext context);
}
