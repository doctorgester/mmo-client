package core.board;

public enum ControlType {
	STUN,
	ROOT,
	SILENCE,
	DISARM
}
